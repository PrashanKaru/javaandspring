package com.hackathon.demo.controller;

import com.hackathon.demo.entities.StockData;
import com.hackathon.demo.service.StockDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/stockdata")
public class StockDataController {

    @Autowired
    private StockDataService stockDataService;

    @Autowired
    StockDataController stockDataRepository;

    @GetMapping
    public List<StockData> findAll() {
        return stockDataService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<StockData> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<StockData>(stockDataService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/stock-ticker/{stockTicker}")
    public ResponseEntity<List<StockData>> findByStockTicker(@PathVariable String stockTicker) {
        try {
            return new ResponseEntity<List<StockData>>(stockDataService.findByStockTicker(stockTicker), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/price-between")
    public ResponseEntity<List<StockData>> findByPriceBetween(@RequestParam double start, @RequestParam double end) {
        try {
            return new ResponseEntity<List<StockData>>(stockDataService.findByPriceBetween(start, end), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/volume-between")
    public ResponseEntity<List<StockData>> findByVolumeBetween(@RequestParam int start, @RequestParam int end) {
        try {
            return new ResponseEntity<List<StockData>>(stockDataService.findByVolumeBetween(start, end), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/buy-or-sell/{act}")
    public ResponseEntity<List<StockData>> findByAct(@PathVariable String act) {
        try {
            return new ResponseEntity<List<StockData>>(stockDataService.findByAct(act), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/status-code/{statusCode}")
    public ResponseEntity<List<StockData>> findByStatusCode(@PathVariable int statusCode) {
        try {
            return new ResponseEntity<List<StockData>>(stockDataService.findByStatusCode(statusCode), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/date")
    public ResponseEntity<List<StockData>> findByDateBetween(@RequestParam Date start, @RequestParam Date end) {
        try {
            return new ResponseEntity<List<StockData>>(stockDataService.findByDateBetween(start, end), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/portfolio-pie")
    public ResponseEntity<List<Map>> aggregatePortfolio() {
        try {
            return new ResponseEntity<List<Map>>(stockDataService.aggregatePortfolio(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<StockData> create(@RequestBody StockData stockData) {
        return new ResponseEntity<StockData>(stockDataService.create(stockData), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<StockData> update(@RequestBody StockData stockData) {
        return new ResponseEntity<StockData>(stockDataService.update(stockData), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        try {
            stockDataService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }


}
