# API Reference

### findAll()
GET /api/v1/stockdata

### update()
PUT /api/v1/stockdata

### create() 
POST /api/v1/stockdata

### delete(@PathVariable id)
DELETE /api/v1/stockdata/{id}

### findById(@PathVariable id)
GET /api/v1/stockdata/{id}

### findByAct(@PathVariable act)
GET /api/v1/stockdata/buy-or-sell/{act}

### findByPriceBetween(@RequestParam start, @RequestParam end)
GET /api/v1/stockdata/price-between

### findByStockTicker(PathVariable stockTicker)
GET /api/v1/stockdata/stock-ticker/{stockTicker}

### findByVolumeBetween(@RequestParam start, @RequestParam end)
GET /api/v1/stockdata/volume-between

### findByStatusCode(@PathVariable statusCode)
GET /api/v1/stockdata/status-code/{statusCode}

### findByDateBetween(@RequestParam start, @RequestParam end)
GET /api/v1/stockdata/date
Valid date format: YYYY-MM-DD

