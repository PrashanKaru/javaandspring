package com.hackathon.demo.repository;

import com.hackathon.demo.entities.StockData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.List;


@Component
public interface StockDataRepository extends JpaRepository<StockData,Long> {

    List<StockData> findStockDataByStockTicker(String stockTicker);

    List<StockData> findStockDataByPriceBetween(double start, double end);

    List<StockData> findStockDataByVolumeBetween(int start, int end);

    List<StockData> findStockDataByAct(String act);

    List<StockData> findStockDataByStatusCode(int statusCode);

    List<StockData> findStockDataByDateBetween(Date start, Date end);

    List<StockData> findStockDataByStatusCodeOrderByStockTicker(int statusCode);

}
