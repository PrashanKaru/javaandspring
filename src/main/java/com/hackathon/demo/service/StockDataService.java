package com.hackathon.demo.service;


import com.hackathon.demo.entities.StockData;
import com.hackathon.demo.repository.StockDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StockDataService {

    @Autowired
    StockDataRepository stockDataRepository;

    public List<StockData> findAll() {
        return stockDataRepository.findAll();
    }

    @GetMapping("/{id}")
    public StockData findById(long id) {
        return stockDataRepository.findById(id).get();
    }

    public StockData create(StockData stockData) {
        return stockDataRepository.save(stockData);
    }

    public List<StockData> findByStockTicker(String stockTicker) {
        return stockDataRepository.findStockDataByStockTicker(stockTicker);
    }

    public List<StockData> findByPriceBetween(double start, double end) {
        return stockDataRepository.findStockDataByPriceBetween(start, end);
    }

    public List<StockData> findByVolumeBetween(int start, int end) {
        return stockDataRepository.findStockDataByVolumeBetween(start, end);
    }

    public List<StockData> findByAct(String act) {
        return stockDataRepository.findStockDataByAct(act);
    }

    public List<StockData> findByStatusCode(int statusCode) {
        return stockDataRepository.findStockDataByStatusCode(statusCode);
    }

    public List<StockData> findByDateBetween(Date start, Date end) {
        return stockDataRepository.findStockDataByDateBetween(start, end);
    }

    public StockData update(StockData stockData) {
        return stockDataRepository.save(stockData);
    }

    public void delete(long id) {
        stockDataRepository.deleteById(id);
    }

    public List<Map> aggregatePortfolio() {
        List<StockData> stockData = stockDataRepository.findStockDataByStatusCodeOrderByStockTicker(0);
        List<Map> finalData = new ArrayList<Map>();
        for (StockData s : stockData) {
            if (finalData.isEmpty() || !s.getStockTicker().equals(finalData.get(finalData.size() - 1).get("name"))) {
                Map map = new HashMap<>();
                map.put("value", s.getAct().equals("BUY") ? s.getVolume() : -s.getVolume());
                map.put("name", s.getStockTicker());
                finalData.add(map);
            } else {
                finalData.get(finalData.size() - 1).merge(
                        "value",
                        s.getAct().equals("BUY") ? s.getVolume() : -s.getVolume(),
                        (oldValue, newValue) -> (Integer) oldValue + (Integer) newValue);
            }
        }
        return finalData;
    }

}
